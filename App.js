import React from 'react';

// Use Navigation Ver. 5.7.0
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';

// Screen
import HomePage from './src/screens/homePage/HomePage';
import AddNewNote from './src/screens/addNewNote/AddNewNote';
import DetailNote from './src/screens/detailNote/DetailNote';

const Stack = createStackNavigator();

function StackNav(props) {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="HomePage" headerMode={'none'}>
        <Stack.Screen name="HomePage" component={HomePage} />
        <Stack.Screen name="AddNewNote" component={AddNewNote} />
        <Stack.Screen name="DetailNote" component={DetailNote} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default StackNav;
