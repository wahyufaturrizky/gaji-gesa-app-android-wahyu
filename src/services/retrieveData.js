import {Request} from './requestHeader';
/**
 * @author wahyu fatur rizki | https://gitlab.com/wahyufaturrizky
 * @return { obj }
 * Retrieve Data
 * return async
 */

export const getAllNotes = () => {
  const result = Request().get('/api/v1/notes/all');
  return result;
};

export const addNewNote = (data) => {
  const result = Request().post('/api/v1/notes/add', data);
  return result;
};

export const getNotedById = (id) => {
  const result = Request().get(`/api/v1/notes/${id}`);
  return result;
};
